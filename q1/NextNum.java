package q1;

import java.util.Random;

public class NextNum {
    int[] randomNumbers;
    double[] probRange;

    public NextNum(int[] randomNumbers, double[] probability) {
        if (randomNumbers.length != probability.length) { //  there should be a probability for each random number
            //  throw error
            throw new java.lang.Error("mismatch in input array length");
        }
        
        double[] probRange = new double[probability.length];
        
        for(int i = 0; i < probability.length; i++) {
            if (i == 0) {
                probRange[i] = probability[i];
            }
            else {
                probRange[i] = probRange[i - 1] + probability[i];
            }
        }
        
        if (Math.abs(probRange[probRange.length - 1] - 1) > 0.0001) { //    fair assumption
            //  sum of all probability should be 1
            //  throw error
            throw new java.lang.Error("sum of all probability should be 1");
        }
        
        this.randomNumbers = randomNumbers;
        this.probRange = probRange;
    }

    public int nextNum(double r) {
        int index = 0;
        while(r >= this.probRange[index]) {
            index++;
        }
        return this.randomNumbers[index];
    }

    public int nextNum() {
        if (this.randomNumbers.length == 0) {
            //  empty array
        }
        Random rand = new Random();
        double r = rand.nextDouble();
        return nextNum(r);
    }
}
