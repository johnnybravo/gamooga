package q1;

import java.util.HashMap;
import java.util.Map;

public class TestCases {
    public static void main(String[] args) {
        int[] randomNumbers = new int[] {-1, 0, 1, 2, 3};
        double[] probability = new double[] {0.11, 0.3, 0.48, 0.1, 0.01};
        NextNum nn = new NextNum(randomNumbers, probability);
        
        //  two kind of test cases
        mockRandomFunctionCases(nn);
        
        checkGenerationFrequency(nn, randomNumbers, probability);

        System.out.println("All test cases passed!");
    }
    
    private static void checkGenerationFrequency(NextNum nn, int[] randomNumbers, double[] probability) {
        //  at lease generate 1000 random numbers (fair assumption)
        
        //  these two are like config variables
        int counterLimit = 1000;
        int threshHold = 50;
        
        //  generate the map as in Execute.java and count the
        //  occurrences based in threshold which we decided
        Map<Integer, Integer> map = new HashMap<>();
        int temp;
        for(int i = 0; i < counterLimit; i++) {
            temp = nn.nextNum();
            if (map.containsKey(temp)) {
                map.put(temp, map.get(temp) + 1);
            }
            else {
                map.put(temp, 1);
            }
        }
        
        int actualCount, probableCount;
        for(int i = 0; i < randomNumbers.length; i++) {
            temp = randomNumbers[i];
            actualCount = map.get(temp);
            probableCount = (int) (probability[i] * counterLimit);
            if (Math.abs(actualCount - probableCount) > threshHold) {
                throw new java.lang.Error("test cases failed");
            }
        }
    }
    
    private static void mockRandomFunctionCases(NextNum nn) {
        //  this time we'll mock the random generator function
        //  and will try to see if the expected output if fine
        double[] randomProbs = new double[] {0.1, 0.35, 0.60, 0.2, 0.99};
        int[] randomNumsExpected = new int[] {-1, 0, 1, 0, 3};

        for(int i = 0; i < randomProbs.length; i++) {
            matchResult(randomNumsExpected[i], nn.nextNum(randomProbs[i]));
        }
    }
    
    private static void matchResult(int expectedOutput, int actualOutput) {
        if (expectedOutput != actualOutput) {
            throw new java.lang.Error("test cases failed");
        }
    }
}
