package q1;

import java.util.HashMap;
import java.util.Map;

public class Execute {
    public static void main(String[] args) {
        int[] randomNumbers = new int[] {-1, 0, 1, 2, 3};
        double[] probability = new double[] {0.11, 0.3, 0.48, 0.1, 0.11};
        NextNum nn = new NextNum(randomNumbers, probability);
        
        Map<Integer, Integer> map = new HashMap<>();
        int temp;
        for(int i = 0; i < 100; i++) {
            temp = nn.nextNum();
            if (map.containsKey(temp)) {
                map.put(temp, map.get(temp) + 1);
            }
            else {
                map.put(temp, 1);
            }
        }
        System.out.println(map);
    }
}
