SELECT  O.product_id,
        P.name,
        SUM(O.quantity) as COPIES_SOLD

FROM orders O

RIGHT JOIN product P ON P.product_id = O.product_id

WHERE P.available_from < DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH)

GROUP BY O.product_id
HAVING COPIES_SOLD < 10;