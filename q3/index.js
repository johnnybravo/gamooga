var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

var numOfClient = 0;
var clients = {}
var arrayOfSocketAndStack = [];

io.on('connection', function(socket) {
  
  console.log('new user connected');
  arrayOfSocketAndStack[numOfClient++] = {
    socket, data: []
  };
  socket.emit('new client', {clientId: numOfClient - 1})
  
  socket.on('chat msg', function(msg) {
    console.log(msg);
    let char;
    for(let i = 0; i < msg.text.length; i++) {
      char = msg.text.charAt(i);

      if (char.match(/\w/)) {
        // keep pushing into the stack
        arrayOfSocketAndStack[ msg.clientId ].data.push( char );
        console.log('current state of this client:', arrayOfSocketAndStack[ msg.clientId ].data);
      }
      else if (char === ' ') {
        let word = getWord(arrayOfSocketAndStack[ msg.clientId ].data);
        if (word.trim()) {
          arrayOfSocketAndStack[ msg.clientId ].socket.emit('chat msg', {
            text: word
          });
        }
        arrayOfSocketAndStack[ msg.clientId ].data = [];
        // pop all and emit
      }
      else {
        console.log('Ignoring char:', char);
        //  special char, do nothing
      }
    }
  });
  
  socket.on('disconnect', function() {
    console.log('user disconnected');
  });
});

function getWord(stack) {
  let word = '';
  let char;
  while(char = stack.pop()) {
    word += char;
  }
  return word;
}

http.listen(3000, function(){
  console.log('listening on *:3000');
});
